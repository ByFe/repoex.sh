#!/bin/bash -

function repoex {

	# traps not working inside bash function
	#trap 'cleanup' QUIT EXIT

	OIFS=$IFS
	IFS='
		'
	declare r
	declare d
	ex=false

	# functions

	function cleanup {
		IFS="$OIFS"
	}

	function showhelp {
		u=`cat <<-[EOF]
			  repoex (repo explorer) v1.3.2
			________________________________________________________________________
			
			Bash function for fast switching between repositories by discovering and 
			indexing all nested repositories inside a directory. Also providing simple 
			history functionality to jump back to last visited repository.
			
			usage: repoex [options]
			
			OPTIONS:
			    -h        show help message.
			    -b        jump back to last repository folder stored in history.
			    -d <path> directory to explore (current directory when not passed).
			    -r        refreshes Index File for current directory or 
			              directory specified via -d argument.
			[EOF]`
		echo -e "\n$u"
	}

	function processArgs {
		local OPTIND;
		while getopts ":hbd:r" OPTION; do
			case ${OPTION} in
				h|H) showhelp; ex=true; ;;
				b|B) historyMoveBack; ex=true; ;;
				d|D) d="$OPTARG"; ;;
				r|R) r=1; ;;
				:)	echo -e "\n!!! ERR: option \"-$OPTARG\" requires an argument."; showhelp; ex=true; ;;
				\?) echo -e "\n!!! ERR: unknown param \"-$OPTARG\"."; showhelp; ex=true; ;;
			esac
		done
		
		#shift $(($OPTIND - 1))
	}

	function pathwin2posix {
		echo "/$1" | sed 's/\\/\//g' | sed 's/://'
	}

	function pathposix2win {
		echo "$1" | sed 's/^\///' | sed 's/\//\\/g' | sed 's/^./\0:/'
	}

	function abspath { 
		p="$1"
		# workaround for bash under windows passing windows style absolute path
		if [[ $p == *":"* ]]; then p=`pathwin2posix "$p"`; fi
		case "$p" in 
			/*)
				printf "%s\n" "$p"
				;;
			*)
				printf "%s\n" "$PWD/$p"
				;;
		esac
	}

	function removeLastLine {
		if [ $# -gt 0 ]; then
			filepath=
			deleteTempFile=false
			tempFileSuffix=".bak"

			[ "$1" != "" ] && filepath="$1"
			[ $# -gt 1 ] && deleteTempFile=$2;

			sed -i"$tempFileSuffix" '$d' "$filepath"

			[ $deleteTempFile == true ] && rm -f "$filepath$tempFileSuffix"
		fi
	}

	function getLastLine {
		[ "$1" != "" ] && sed '$!d' "$1"
	}

	function removeFirstLine {
		if [ $# -gt 0 ]; then
			filepath=
			deleteTempFile=false
			tempFileSuffix=".bak"

			[ "$1" != "" ] && filepath="$1"
			[ $# -gt 1 ] && deleteTempFile=$2;

			sed -i"$tempFileSuffix" '1d' "$filepath"

			[ $deleteTempFile == true ] && rm -f "$filepath$tempFileSuffix"
		fi
	}

	function countLines {
		if [ "$1" != "" ]; then
			awk 'END{ print FNR }' "$1"
		fi
	}

	function printIndex {
		awk -F'|' '{ print "["sprintf("%02d", NR)"]"" "$1 }' $DIRIndexFilePath
	}

	function repoPathByIndexFileLineNumber {
		if [ "$1" != "" ]; then
			awk -F'|' -v lineNumber=$1 'NR == lineNumber { print $2 }' $DIRIndexFilePath
		fi
	}

	function repoNameByIndexFileLineNumber {
		if [ "$1" != "" ]; then
			awk -F'|' -v lineNumber=$1 'NR == lineNumber { print $1 }' $DIRIndexFilePath
		fi
	}

	function repoNameByPath {
		if [ "$1" != "" ]; then
			awk -F'|' -v repoPath="$1" '$2 == repoPath { print $1 }' $DIRIndexFilePath
		fi
	}

	function repoPathByName {
		if [ "$1" != "" ]; then
			awk -F'|' -v repoName="$1" '$1 == repoName { print $2 }' $DIRIndexFilePath
		fi
	}

	function clearHistory {
		> $HISTORYFILEPATH
	}

	function pushPathToHistory {
		if [ "$1" != "" ]; then
			path="$1"
			
			[ ! -f $HISTORYFILEPATH ] && touch $HISTORYFILEPATH

			[ "`getLastLine $HISTORYFILEPATH`" == "$path" ] && return

			historyItemCount=`countLines $HISTORYFILEPATH`
			[ $historyItemCount -ge $MAXHISTORYITEMS ] && removeFirstLine $HISTORYFILEPATH true

			echo "$path" >> $HISTORYFILEPATH
		fi
	}

	function historyMoveBack {
		if [ -f $HISTORYFILEPATH ]; then
			historyItemCount=`countLines $HISTORYFILEPATH`
			if [ $historyItemCount -gt 0 ]; then
				# pop last entry
				lastPath=`getLastLine $HISTORYFILEPATH`
				removeLastLine $HISTORYFILEPATH true
				echo "switching back to directory "
				echo " > $lastPath"
				cd "$lastPath"
			else 
				echo "INFO: History is empty."
			fi
		fi
	}

	# snoitcnuf

	# configure paths / defaults and handle params
	WORKINGDIR=~/".repoex"
	INDEXFILESDIR="$WORKINGDIR"
	INDEXTABLEFILE="$INDEXFILESDIR/index"
	TMPDIR="$WORKINGDIR/tmp"
	HISTORYFILENAME="HISTORY"
	HISTORYFILEPATH="$TMPDIR/$HISTORYFILENAME"
	MAXHISTORYITEMS=10
	DELIMITER="|"


	processArgs "$@"
	[ $ex == true ] && return


	DIR="$PWD"
	[ ! -z "$d" ] && DIR="$d"
	ABSDIR=`abspath "$DIR"`
	REFRESHINDEX=false
	[ ! -z "$r" ] && REFRESHINDEX=true

	# ensure existence of required directories/files
	[ ! -d "$WORKINGDIR" ] && mkdir -p $WORKINGDIR
	[ ! -d "$INDEXFILESDIR" ] && mkdir -p $INDEXFILESDIR
	[ ! -d "$TMPDIR" ] && mkdir -p $TMPDIR
	[ ! -f $INDEXTABLEFILE ] && touch $INDEXTABLEFILE

	CreateNewIndexFile=true

	if [ -d "$ABSDIR" ]; then

		# TODO?: check modification date of index file
		#		 when older than max age specified in defaults
		#		 refresh too

		IFS=$DELIMITER
		while read id path; do
			if [ "$path" == "$ABSDIR" ]; then
				CreateNewIndexFile=false
				DIRIndexFileID=$id
			fi
		done < $INDEXTABLEFILE
		IFS=$OIFS

		if [ $CreateNewIndexFile == true ]; then
			DIRIndexFileID=`uuidgen`
			# create new index file with entry in index table
			touch "$INDEXFILESDIR/$DIRIndexFileID"
			echo "$DIRIndexFileID$DELIMITER$ABSDIR" >> $INDEXTABLEFILE
			REFRESHINDEX=true
		fi

		DIRIndexFilePath="$INDEXFILESDIR/$DIRIndexFileID"

		if [ $REFRESHINDEX == true ]; then
			echo "refreshing index for directory: $ABSDIR" 

			# clear index file
			> $DIRIndexFilePath

			# refill index
			IFS=$'\n'
			for d in `find "$ABSDIR" -name *.git -prune`; do
				
				# detect bare repo
				if [ ${d:${#d}-5:1} == '/' ]; then
					# is repo with working copy (is directory or file with name .git):
					# strip trailing /.git from path
					d=${d:0:${#d}-5}
					# repo name = name of directory which contains .git file/folder
					name=`basename "$d"`
				elif [[ -d $d ]]; then
					# is bare repo (is directory with name ending on .git):
					# leave path as is but strip .git form basename
					name=`basename "$d"`
					name=${name:0:${#name}-4}
				else
					# ignore unwanted file
					# (file within subfolder with name ending on .git)
					continue
				fi
				
				# make it absolute if its not already
				#dabs=`abspath $d`

				# append found repo to index file
				echo "$name$DELIMITER$d" >> $DIRIndexFilePath
			done
			IFS=$OIFS
		fi

		indexrecordscount=`countLines "$DIRIndexFilePath"`

		while true; do

			printIndex
			echo "[qQ] quit"
			read -p "jump to [1..$(($indexrecordscount))|qQ]... " input

			echo -e

			case $input in
				[0-9]*)
					if [[ $input -ge 1 && $input -le $indexrecordscount ]]; then
				
						# push current working directory to history
						pushPathToHistory "$PWD"
						
						selectedRepoName=`repoNameByIndexFileLineNumber $input`
						selectedRepoPath=`repoPathByIndexFileLineNumber $input`

						echo "switching to [$selectedRepoName]"
						echo " > $selectedRepoPath"
						cd "$selectedRepoPath"
						return
					
					else
						echo "...you must enter a number between 1 and $(($indexrecordscount))"
					fi
					;;
				[qQ])
					echo "bye"
					break
					;;
				*) 
					echo "...you must enter something"
					;;
			esac

		done

	else
		echo "!!! ERR: specified path doesn't exist!"
		echo "!!!      [$ABSDIR]"
	fi

	cleanup
}